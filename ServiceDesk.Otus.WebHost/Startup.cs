using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Settings;
using ServiceDesk.Otus.DataAccess.DbContexts;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using ServiceDesk.Otus.Core.Data.Domain;
using ServiceDesk.Otus.DataAccess.Repositories;
using ServiceDesk.Otus.WebHost.Models;
using ServiceDesk.Otus.DataAccess.Data;

namespace ServiceDesk.Otus.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            AddDbContext(services);
            
            services.AddOpenApiDocument(options =>
            {
                options.Title = "ServiceDesk API Doc";
                options.Version = "1.0";
            });

        }

        private void AddDbContext(IServiceCollection services)
        {
            var connectionStrings = Configuration.Get<ConnectionStrings>();

            services.AddScoped<IRepository<Role>, Repository<Role>>();
            services.AddScoped<IRepository<User>, Repository<User>>();
            services.AddScoped<IRepository<Request>, Repository<Request>>();
            services.AddScoped<IRepository<RequestHistory>, Repository<RequestHistory>>();
            services.AddScoped<IRepository<Status>, Repository<Status>>();

            services.AddDbContext<DataContext>(
                options => {
                    
                    options.UseNpgsql(Configuration.GetConnectionString("DefaultConnection"), sqlOptions =>
                            sqlOptions.MigrationsAssembly(
                                typeof(DataContext).GetTypeInfo().Assembly.GetName().Name)
                        );

                    
            });

            services.AddScoped<IUnitOfWork>(x => x.GetRequiredService<DataContext>());
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            //���������� ������
            using (var scope = app.ApplicationServices.CreateScope())
            {
                DataContext context = scope.ServiceProvider.GetRequiredService<DataContext>();

                
                context.Database.EnsureDeleted();
                
                context.Database.EnsureCreated();
                context.Status.AddRange(FakeData.Statuses);
                //context.Roles.AddRange(FakeData.Roles);
                context.Users.AddRange(FakeData.Users);
                

                
                context.SaveChanges();
            }

            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            
            }
    }
}
