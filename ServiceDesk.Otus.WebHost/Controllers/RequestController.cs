﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Data.Domain;
using ServiceDesk.Otus.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.WebHost.Controllers
{
    /// <summary>
    /// Заявки
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RequestController : ControllerBase
    {
        private readonly IRepository<Request> _repository;
        private readonly IRepository<Status> _statusRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public RequestController(
            IRepository<Request> repository,
            IRepository<Status> statusRepository,
            IRepository<User> userRepository,
            IUnitOfWork unitOfWork
        )
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _statusRepository = statusRepository;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Получить заявки согласно условиям фильтра
        /// </summary>
        [HttpGet("list")]
        public async Task<IEnumerable<RequestItemResponse>> GetRequestsAsync(
            [FromQuery] RequestFilter filter
        )
        {
            var users = await _repository.GetAllAsync();

            var responseQuery = await users
                .Skip(filter.Skip)
                .Take(filter.Take)
                .Include(property => property.Applicant)
                .Include(property => property.Contractor)
                .Include(property => property.Creator)
                .Include(property => property.Status)
                .ToListAsync();

            var result = responseQuery.Select(item =>
            {
                UserItemShortResponse applicant = null;
                if (item.Applicant != null)
                {
                    applicant = new UserItemShortResponse
                    {
                        Id = item.Applicant.Id,
                        FirstName = item.Applicant.FirstName,
                        LastName = item.Applicant.LastName,
                    };
                }

                UserItemShortResponse contractor = null;
                if (item.Contractor != null)
                {
                    contractor = new UserItemShortResponse
                    {
                        Id = item.Contractor.Id,
                        FirstName = item.Contractor.FirstName,
                        LastName = item.Contractor.LastName,
                    };
                }

                UserItemShortResponse creator = null;
                if (item.Creator != null)
                {
                    creator = new UserItemShortResponse
                    {
                        Id = item.Creator.Id,
                        FirstName = item.Creator.FirstName,
                        LastName = item.Creator.LastName,
                    };
                }

                StatusItemResponse status = null;
                if (item.Status != null)
                {
                    status = new StatusItemResponse
                    {
                        Id = item.Status.Id,
                        Name = item.Status.NameStatus,
                    };
                }

                var request = new RequestItemResponse
                {
                    Id = item.Id,
                    Applicant = applicant,
                    Contractor = contractor,
                    Creator = creator,
                    Status = status,
                    Description = item.Description,
                    ExpirationDate = item.ExpirationDate,
                    Title = item.Title,
                    CloseDate = item.CloseDate,
                    CreateDate = item.CreateDate,
                };

                return request;
            }).ToList();

            return result;
        }

        /// <summary>
        /// Получить заявку по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(UserItemResponse), StatusCodes.Status200OK)]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RequestItemResponse>> GetAsync(
            Guid id
        )
        {
            var entity  = await _repository.GetByIdAsync(id);

            if (entity  == null)
            {
                return NotFound();
            }
            //TODO: если не будет ленивой загрузки, то не будет работать.
            UserItemShortResponse applicant = null;
            if (entity.Applicant != null)
            {
                applicant = new UserItemShortResponse
                {
                    Id = entity.Applicant.Id,
                    FirstName = entity.Applicant.FirstName,
                    LastName = entity.Applicant.LastName,
                };
            }

            UserItemShortResponse contractor = null;
            if (entity.Contractor != null)
            {
                contractor = new UserItemShortResponse
                {
                    Id = entity.Contractor.Id,
                    FirstName = entity.Contractor.FirstName,
                    LastName = entity.Contractor.LastName,
                };
            }

            UserItemShortResponse creator = null;
            if (entity.Creator != null)
            {
                creator = new UserItemShortResponse
                {
                    Id = entity.Creator.Id,
                    FirstName = entity.Creator.FirstName,
                    LastName = entity.Creator.LastName,
                };
            }

            StatusItemResponse status = null;
            if (entity.Status != null)
            {
                status = new StatusItemResponse
                {
                    Id = entity.Status.Id,
                    Name = entity.Status.NameStatus,
                };
            }

            var result = new RequestItemResponse
            {
                Id = entity.Id,
                Applicant = applicant,
                Contractor = contractor,
                Creator = creator,
                Status = status,
                Description = entity.Description,
                ExpirationDate = entity.ExpirationDate,
                Title = entity.Title,
                CloseDate = entity.CloseDate,
                CreateDate = entity.CreateDate,
            };

            return Ok(result);
        }

        /// <summary>
        /// Добавить новую заявку
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPost]
        public async Task<ActionResult> CreateAsync(
            [FromBody] CreateRequest request
        )
        {
            var status = await _statusRepository.GetByIdAsync(request.StatusId);
            
            if (status == null)
            {
                throw new ArgumentNullException(nameof(status));
            }

            var applicant = await _userRepository.GetByIdAsync(request.ApplicantId);

            if (applicant == null)
            {
                throw new ArgumentNullException(nameof(applicant));
            }

            var contractor = await _userRepository.GetByIdAsync(request.ContractorId);

            if (contractor == null)
            {
                throw new ArgumentNullException(nameof(contractor));
            }

            var creator = await _userRepository.GetByIdAsync(request.CreatorId);

            if (creator == null)
            {
                throw new ArgumentNullException(nameof(creator));
            }

            var id = Guid.NewGuid();

            var entity = new Request
            {
                Id = id,
                Applicant = applicant,
                Contractor = contractor,
                Creator = creator,
                Status = status,
                Description = request.Description,
                ExpirationDate = request.ExpirationDate ?? DateTime.UtcNow.AddDays(14), //TODO: в настройки?
                Title = request.Title,
                CloseDate = null,
                CreateDate = DateTime.UtcNow,
            };

            _repository.Add(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Изменить заявку
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="request">Данные, которые надо изменить</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> EditAsync(
            [FromRoute] Guid id, 
            [FromBody] EditRequest request
        )
        {
            var entity = await _repository.GetByIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            if (request.StatusId.HasValue)
            {
                var status = await _statusRepository.GetByIdAsync(request.StatusId.Value);

                if (status == null)
                {
                    throw new ArgumentNullException(nameof(status));
                }

                entity.Status = status;
            }

            if (request.ContractorId.HasValue)
            {
                var contractor = await _userRepository.GetByIdAsync(request.ContractorId.Value);

                if (contractor == null)
                {
                    throw new ArgumentNullException(nameof(contractor));
                }

                entity.Contractor = contractor;
            }

            if (request.ExpirationDate > DateTime.UtcNow)
            {
                entity.ExpirationDate = request.ExpirationDate.Value;
            }

            _repository.Update(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Закрыть заявку
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPatch("{id:guid}")]
        public async Task<IActionResult> CloseAsync(
            [FromRoute] Guid id
        )
        {
            var entity = await _repository.GetByIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            entity.CloseDate = DateTime.UtcNow;

            _repository.Update(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Удалить заявку по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var entityToDelete = await _repository.GetByIdAsync(id);

            if (entityToDelete == null)
            {
                return NotFound();
            }

            _repository.Remove(entityToDelete);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }
    }
}
