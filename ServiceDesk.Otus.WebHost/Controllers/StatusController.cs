﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Data.Domain;
using ServiceDesk.Otus.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.WebHost.Controllers
{
    /// <summary>
    /// Статусы
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class StatusController : ControllerBase
    {
        private readonly IRepository<Status> _repository;
        private readonly IUnitOfWork _unitOfWork;

        public StatusController(
            IRepository<Status> repository, 
            IUnitOfWork unitOfWork
        )
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить все доступные статусы
        /// </summary>
        [HttpGet("list")]
        public async Task<IEnumerable<StatusItemResponse>> GetStatusesAsync()
        {
            var statuses = await _repository.GetAllAsync();

            var result = statuses.Select(item =>
                new StatusItemResponse
                {
                    Id = item.Id,
                    Name = item.NameStatus,
                }).ToList();

            return result;
        }

        /// <summary>
        /// Получить статус по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(StatusItemResponse), StatusCodes.Status200OK)]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<StatusItemResponse>> GetAsync(Guid id)
        {
            var entity  = await _repository.GetByIdAsync(id);

            if (entity  == null)
            {
                return NotFound();
            }

            var result = new StatusItemResponse
            {
                Id = entity.Id,
                Name = entity.NameStatus,
            };

            return Ok(result);
        }

        /// <summary>
        /// Добавить новый статус
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPost]
        public async Task<ActionResult> CreateAsync([FromBody] CreateStatusRequest request)
        {
            var id = Guid.NewGuid();

            var entity = new Status
            {
                Id = id,
                NameStatus = request.Name,
            };

            _repository.Add(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Удалить статус по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var entityToDelete = await _repository.GetByIdAsync(id);

            if (entityToDelete == null)
            {
                return NotFound();
            }

            _repository.Remove(entityToDelete);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }
    }
}
