﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Data.Domain;
using ServiceDesk.Otus.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IRepository<User> _repository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UserController(
            IRepository<User> repository,
            IUnitOfWork unitOfWork,
            IRepository<Role> roleRepository
        )
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Получить список сотрудников согласно условиям фильтра
        /// </summary>
        [HttpGet("list")]
        public async Task<IEnumerable<UserItemResponse>> GetUsersAsync(
            [FromQuery] UserFilter filter
        )
        {
            var users = await _repository.GetAllAsync();

            var responseQuery = await users
                .Skip(filter.Skip)
                .Take(filter.Take)
                .ToListAsync();

            var result = responseQuery.Select(item =>
            {
                RoleItemResponse role = null;
                if (item.Role != null)
                {
                    role = new RoleItemResponse
                    {
                        Id = item.Role.Id,
                        Name = item.Role.NameRole,
                        AccessRights = item.Role.AccessRights,
                    };
                }

                var user = new UserItemResponse
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Email = item.Email,
                    Login = item.Login,
                    PhoneNum = item.PhoneNum,
                    Role = role,
                };

                return user;
            }).ToList();

            return result;
        }

        /// <summary>
        /// Получить сотрудника по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(UserItemResponse), StatusCodes.Status200OK)]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<UserItemResponse>> GetAsync(Guid id)
        {
            var entity  = await _repository.GetByIdAsync(id);

            if (entity  == null)
            {
                return NotFound();
            }

            RoleItemResponse role = null;
            if (entity.Role != null)
            {
                role = new RoleItemResponse
                {
                    Id = entity.Role.Id,
                    Name = entity.Role.NameRole,
                    AccessRights = entity.Role.AccessRights,
                };
            }

            var result = new UserItemResponse
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Email = entity.Email,
                Login = entity.Login,
                PhoneNum = entity.PhoneNum,
                Role = role,
            };

            return Ok(result);
        }

        /// <summary>
        /// Добавить нового сотрудника
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPost]
        public async Task<ActionResult> CreateAsync(
            [FromBody] CreateOrEditUserRequest request
        )
        {
            Role role = null;
            if (request.RoleId.HasValue)
            {
                role = await _roleRepository.GetByIdAsync(request.RoleId.Value);
            }

            var id = Guid.NewGuid();

            var entity = new User
            {
                Id = id,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Login = request.Login,
                PhoneNum = request.PhoneNum,
                Password = request.Password,
                Role = role,
            };

            _repository.Add(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Обновить информацию о сотруднике
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="request">Данные, которые надо обновить</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditAsync(
            [FromRoute] Guid id, 
            [FromBody] CreateOrEditUserRequest request
        )
        {
            var entity = await _repository.GetByIdAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            if (request.RoleId.HasValue)
            {
                entity.Role = await _roleRepository.GetByIdAsync(request.RoleId.Value);
            }

            entity.FirstName = request.FirstName;
            entity.LastName = request.LastName;
            entity.Email = request.Email;
            entity.PhoneNum = request.PhoneNum;

            _repository.Update(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Удалить сотрудника по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var entityToDelete = await _repository.GetByIdAsync(id);

            if (entityToDelete == null)
            {
                return NotFound();
            }

            _repository.Remove(entityToDelete);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }
    }
}
