﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Data.Domain;
using ServiceDesk.Otus.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RoleController : ControllerBase
    {
        private readonly IRepository<Role> _repository;
        private readonly IUnitOfWork _unitOfWork;

        public RoleController(
            IRepository<Role> repository, 
            IUnitOfWork unitOfWork
        )
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        [HttpGet("list")]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _repository.GetAllAsync();

            var result = roles.Select(item =>
                new RoleItemResponse
                {
                    Id = item.Id,
                    Name = item.NameRole,
                    AccessRights = item.AccessRights
                }).ToList();

            return result;
        }

        /// <summary>
        /// Получить роль по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(RoleItemResponse), StatusCodes.Status200OK)]
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleItemResponse>> GetAsync(Guid id)
        {
            var entity  = await _repository.GetByIdAsync(id);

            if (entity  == null)
            {
                return NotFound();
            }

            var result = new RoleItemResponse
            {
                Id = entity.Id,
                Name = entity.NameRole,
                AccessRights = entity.AccessRights
            };

            return Ok(result);
        }

        /// <summary>
        /// Добавить новую роль
        /// </summary>
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpPost]
        public async Task<ActionResult> CreateAsync([FromBody] CreateRoleRequest request)
        {
            var id = Guid.NewGuid();

            var entity = new Role
            {
                Id = id,
                NameRole = request.Name,
                AccessRights = request.AccessRights,
            };

            _repository.Add(entity);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        /// <summary>
        /// Удалить роль по идентификатору
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            var entityToDelete = await _repository.GetByIdAsync(id);

            if (entityToDelete == null)
            {
                return NotFound();
            }

            _repository.Remove(entityToDelete);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }
    }
}
