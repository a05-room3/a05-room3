﻿using System;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class UserItemShortResponse
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
