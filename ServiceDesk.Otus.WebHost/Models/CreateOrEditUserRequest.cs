﻿using Destructurama.Attributed;
using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class CreateOrEditUserRequest
    {
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(50)]
        [Phone]
        public string PhoneNum { get; set; }

        [MaxLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Login { get; set; }

        [LogMasked]
        public string Password { get; set; }

        public Guid? RoleId { get; set; }
    }
}
