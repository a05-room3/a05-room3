﻿using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class UserFilter
    {
        [Required]
        public int Skip { get; set; }

        [Required]
        public int Take { get; set; }
    }
}
