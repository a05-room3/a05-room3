﻿using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.WebHost.Models
{
    /// <summary>
    /// Запрос на создание новой роли
    /// </summary>
    public class CreateRoleRequest
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public byte AccessRights { get; set; }
    }
}
