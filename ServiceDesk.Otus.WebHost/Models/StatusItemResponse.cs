﻿using System;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class StatusItemResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
