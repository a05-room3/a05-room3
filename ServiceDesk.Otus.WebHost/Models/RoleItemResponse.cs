﻿using System;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class RoleItemResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public byte AccessRights { get; set; }
    }
}
