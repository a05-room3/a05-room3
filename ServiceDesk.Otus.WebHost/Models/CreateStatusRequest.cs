﻿using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.WebHost.Models
{
    /// <summary>
    /// Запрос на создание нового статуса
    /// </summary>
    public class CreateStatusRequest
    {
        [MinLength(3)]
        [MaxLength(20)]        
        public string Name { get; set; }
    }
}
