﻿using System;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class RequestItemResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public UserItemShortResponse Contractor { get; set; } //Исполнитель
        public UserItemShortResponse Applicant { get; set; } // Заявитель
        public UserItemShortResponse Creator { get; set; } // Создатель
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public StatusItemResponse Status { get; set; }
    }
}
