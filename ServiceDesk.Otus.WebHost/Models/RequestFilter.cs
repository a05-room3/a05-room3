﻿using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class RequestFilter
    {
        [Required]
        public int Skip { get; set; }

        [Required]
        public int Take { get; set; }
    }
}
