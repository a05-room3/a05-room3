﻿using System;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class EditRequest
    {
        public Guid? ContractorId { get; set; } //Исполнитель
        public DateTime? ExpirationDate { get; set; }
        public Guid? StatusId { get; set; }
    }
}
