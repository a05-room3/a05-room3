﻿using System;

namespace ServiceDesk.Otus.WebHost.Models
{
    public class CreateRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid ContractorId { get; set; } //Исполнитель
        public Guid ApplicantId { get; set; } // Заявитель
        public Guid CreatorId { get; set; } // Создатель
        public DateTime? ExpirationDate { get; set; }
        public Guid StatusId { get; set; }
    }
}
