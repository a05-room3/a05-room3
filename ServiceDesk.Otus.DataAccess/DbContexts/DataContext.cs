﻿using Microsoft.EntityFrameworkCore;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Data.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.DataAccess.DbContexts
{
    public class DataContext : DbContext, IUnitOfWork
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Request> Requests { get; set; }
        public DbSet<RequestHistory> RequestsHistory { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Status> Status { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }

        public Task SaveAsync(CancellationToken cancellationToken = default)
        {
            return SaveChangesAsync(cancellationToken);
        }
    }
}
