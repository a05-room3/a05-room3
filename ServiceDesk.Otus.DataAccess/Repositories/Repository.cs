﻿using Microsoft.EntityFrameworkCore;
using ServiceDesk.Otus.Core.Abstractions.Repositories;
using ServiceDesk.Otus.Core.Data;
using ServiceDesk.Otus.DataAccess.DbContexts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.DataAccess.Repositories
{
    public class Repository<T>
           : IRepository<T>
           where T : BaseEntity
    {
        private readonly DataContext _dataContext;
        private readonly DbSet<T> _database;

        public Repository(DataContext dataContext)
        {
            _dataContext = dataContext ?? throw new ArgumentNullException(nameof(dataContext));

            _database = _dataContext.Set<T>();
        }

        public Task<IQueryable<T>> GetAllAsync()
        {
            return Task.FromResult(_database.AsQueryable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(_database.Find(id));
        }

        public void Update(T entity)
        {
            if (_dataContext.Entry(entity).State == EntityState.Detached)
            {
                _database.Attach(entity);
            }

            _database.Update(entity);
        }

        public void Add(T entity)
        {
            if (_dataContext.Entry(entity).State == EntityState.Detached)
            {
                _database.Attach(entity);
            }

            _database.Add(entity);
        }

        public void Remove(T entity)
        {
            if (_dataContext.Entry(entity).State == EntityState.Detached)
            {
                _database.Attach(entity);
            }

            _database.Remove(entity);
        }
    }
}
