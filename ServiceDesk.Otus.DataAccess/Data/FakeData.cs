﻿using ServiceDesk.Otus.Core.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceDesk.Otus.DataAccess.Data
{
    public static class FakeData
    {

        public static IEnumerable<Status> Statuses => new List<Status>()
        {
            new Status()
            {
                Id = Guid.Parse("{9E29E58E-494D-401A-978D-AA3F6448139B}"),
                NameStatus = "Создано"
            },
            new Status()
            {
                Id = Guid.Parse("{55FA46C6-66C0-4A16-B0EA-1FC51FE3B258}"),
                NameStatus = "В работе"
            },
            new Status()
            {
                Id = Guid.Parse("{353EB7CB-24FE-427B-B472-C6563D812F2D}"),
                NameStatus = "Выполнено"
            },
            new Status()
            {
                Id = Guid.Parse("{91080BC7-1A23-4DAD-9267-076AAB3BF770}"),
                NameStatus = "Закрыто"
            }
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id= Guid.Parse("{5CEF342C-A6EE-4D33-BA03-62864A7A20A7}"),
                NameRole="Менеджер",
                AccessRights= 1
            },
            new Role()
            {
                Id= Guid.Parse("{21FC0DC0-E75D-4585-9B62-A62DA9F3CEC1}"),
                NameRole="Пользователь",
                AccessRights= 10
            },
            new Role()
            {
                Id= Guid.Parse("{F311F8C5-79F5-40EB-949D-DF5C83B7BFFD}"),
                NameRole="Специалист",
                AccessRights= 100
            }
        };

        public static IEnumerable<User> Users => new List<User>()
        {
            new User()
            {
                Id = Guid.Parse("{B50315E5-408D-472D-995C-EF8177A176B5}"),
                FirstName="Ольга",
                LastName="Иванова",
                Email="ivanova@servicedesk.net",
                Login="oivanova",
                Password="",
                PhoneNum="32165498",
                Role = Roles.FirstOrDefault(x => x.NameRole=="Менеджер")
            },
            new User()
            {
                Id = Guid.Parse("{D6B3A5F4-3CB4-4602-90FE-325A06CD5A1F}"),
                FirstName="Андрей",
                LastName="Сидоров",
                Email="sidorov@superpochta.net",
                Login="asidorov",
                Password="",
                PhoneNum="45645645",
                Role = Roles.FirstOrDefault(x => x.NameRole=="Пользователь")
            },
            new User()
            {
                Id = Guid.Parse("{00DFD0E2-0199-4E82-AF82-D67AAE658C00}"),
                FirstName="Иван",
                LastName="Петров",
                Email="petrov@servicedesk.net",
                Login="ipetrov",
                Password="",
                PhoneNum="2342345",
                Role = Roles.FirstOrDefault(x => x.NameRole=="Специалист")
            }
        };
    }
}
