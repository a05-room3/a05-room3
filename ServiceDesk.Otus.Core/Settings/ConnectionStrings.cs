﻿namespace ServiceDesk.Otus.Core.Settings
{
    /// <summary>
    /// Настройки со строками подключения к БД
    /// </summary>

    public class ConnectionStrings
    {
        /// <summary>
        /// Подключение к БД 
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
