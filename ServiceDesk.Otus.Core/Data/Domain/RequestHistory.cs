﻿using System;

namespace ServiceDesk.Otus.Core.Data.Domain
{
    public class RequestHistory : BaseEntity
    {

        public Request Request { get; set; }
        
        public string Description { get; set; }
        public User Contractor { get; set; } //Исполнитель
        public DateTime CreateDate { get; set; }

        public DateTime CloseDate { get; set; }

        public DateTime ExpirationDate { get; set; }

        public Status Status { get; set; }
        public DateTime Changedate { get; set; }
    }
}
