﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ServiceDesk.Otus.Core.Data.Domain
{
   public class User : BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }
        [MaxLength(50)]
        public string PhoneNum { get; set; }
        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(20)]
        public string Login { get; set; }

        public string Password { get; set; }

        public Role Role { get; set; }
    }
}
