﻿using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.Core.Data.Domain
{
    public class Status : BaseEntity
    {
        [MaxLength(20)]
        public string NameStatus { get; set; }
    }
}
