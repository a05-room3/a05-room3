﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ServiceDesk.Otus.Core.Data.Domain
{
    public class Request : BaseEntity
    {
        [MaxLength(50)]
        public string Title { get; set; }
        public string Description { get; set; }
        public User Contractor { get; set; } //Исполнитель
        public User Applicant { get; set; } // Заявитель
        public User Creator { get; set; } // Создатель
        public DateTime CreateDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public Status Status { get; set; }
    }
}
