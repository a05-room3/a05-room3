﻿namespace ServiceDesk.Otus.Core.Data.Domain
{
    public class Role : BaseEntity
    {
        public string NameRole { get; set; }
        public byte AccessRights { get; set; }
    }
}
