﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServiceDesk.Otus.Core.Data
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }
    }
}
