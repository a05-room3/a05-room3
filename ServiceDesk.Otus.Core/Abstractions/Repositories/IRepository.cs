﻿using ServiceDesk.Otus.Core.Data;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.Core.Abstractions.Repositories
{
    public interface IRepository<T>
            where T : BaseEntity
    {
        Task<IQueryable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        void Update(T entity);

        void Add(T entity);

        void Remove(T entity);
    }
}
