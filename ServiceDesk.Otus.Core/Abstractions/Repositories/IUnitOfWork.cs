﻿using System.Threading;
using System.Threading.Tasks;

namespace ServiceDesk.Otus.Core.Abstractions.Repositories
{
    public interface IUnitOfWork
    {
        public Task SaveAsync(CancellationToken cancellationToken = default);
    }
}
